let result = [];
function flatten(elements)
{
    for(let index=0;index<elements.length;index++)
    {
        if(Number.isInteger(elements[index]))
        {
          result.push(elements[index]);
        }
        else
        {
          flatten(elements[index]);
        }
    }
    return result;
}
module.exports = flatten;